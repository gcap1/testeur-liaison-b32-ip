# Testeur liaison B32 IP 

Outil de simulation développé par le Groupe Contrôle et Automatisation des Postes (GCAP) d'Enedis. Il s'agit d'un script python qui permet de tester la passerelle B32/IP. Cet outil peut être utile dans les tests de qualification des PCCN-IP. Une interface graphique est prévue afin de simplifier l'envoi de commandes. 


## Interface Graphique 
![Capture](/uploads/23d63fdfb9c288d057f36876b2a512d2/Capture.PNG)

## Procédure à suivre

Voir le [Mode Opératoire](Mode_Opératoire.pdf)

## Licence & Copyright

Licensed under the [MIT License](LICENSE)

