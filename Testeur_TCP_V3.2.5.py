from tkinter import *
import socket
import time
import sys
from threading import Thread, currentThread
from datetime import datetime
import configparser

#Numéro de Version
Version="3.2.5"
# 3.2.2 : Correction Nature message Bulle
# 3.2.2 : Correction réception message bulle Passerelle pris pour une TM prioritaire
# 3.2.3 : Prise en compte des messages B32 qui se suivent avec 2 octect de longeur avant
# 3.2.4 : Correction syntaxe
# 3.2.5 : Correction de l'ouverture et la fermeture de la socket
#         Correction Start and Stop envoi de messages bulles

def msgdate(msgx):
    now=datetime.today()
    dx=("0"+str(now.day))[len(str(now.day))-1:]+"/"+("0"+str(now.month))[len(str(now.month))-1:]+"/"+str(now.year)
    hx=("0"+str(now.hour))[len(str(now.hour))-1:]+"h"+("0"+str(now.minute))[len(str(now.minute))-1:]+"m"+("0"+str(now.second))[len(str(now.second))-1:]+"s"+("00"+str(now.microsecond))[len(str(now.microsecond))-1:]
    tx= dx + " " + hx + "  " + msgx
    msgseul(tx)

def msgseul(tx):
    print(tx)
    fichierdetrace=open(nomdufichiertrace, "a")
    fichierdetrace.write("\n"+tx)
    fichierdetrace.close()


def SaveIni():
    global config
    with open(FichIni, 'w') as configfile:    # save
        config.write(configfile)

# Fonctions qui enregistre les modifications lors d'un appui sur la touche "Entrer" de la zone de saisie 
# ////////////////////////////////////////////////////////////////////////////////////////
def execute_quand_entree_zone_ip_sup(event):
    tx=" -> nouvelle adresse IP "+zonedesaisieipsup.get() 
    msgdate(tx)

def execute_quand_entree_zone_TselPa(event):
    global tselpa
    tx=" TSEL avant saisie "+str(tselpa)
    tselpa=zonedesaisieTselPa.get()
    tx=tx+" -> nouveau TSEL "+str(tselpa)
    msgdate(tx)

def execute_quand_entree_zone_port_sup(event):
    global PORT
    #print(str(datetime.now()),"port avant:",PORT)
    tx="port avant: "+str(PORT)
    PORT=int(zonedesaisieportsup.get())
    tx=tx+" -> port apres modif de saisie: "+str(PORT)
    msgdate(tx)
#/////////////////////////////////////////////////////////////////////////////////////////

def entree_Bourage(event):
    global Bour
    Bour=int(zonedesaisieBourage.get())
    tx=" -> Bourrage nombre message passerelle : "+str(Bour)+" octets"
    msgdate(tx)

#Envoi du message quand clic lord d'un clic OK
def envoiemessageb32():
    global bulles 
    bytesaenvoyer=str(zonedesaisieb32.get())
    msgseul(" Envoie de message B32 : "+bytesaenvoyer)
    time.sleep(1)
    thread1.pause()
    TrameB32txt(bytesaenvoyer)
    time.sleep(1)
    if bulles==1:
        thread1.reprise()

#Chaine de caractère de la Trame IP en hexa sans le nb d'octet
def TrameB32txt(bytesaenvoyer):
    global PaIp
    global Bour
    tx = ""
    msgx = [0]
    lg = int(len(bytesaenvoyer)/2)
    lg1 = int(lg/256)
    lg2 = lg-(256*lg1)
    msgx.append(lg2)
    tx = chr(lg1) + chr(lg2)
    tx2=""
    if PaIp == True :
        bytesaenvoyer=MsgBour(Bour, 7)+MsgBour(Bour, 1)+MsgBour(Bour, lg)+bytesaenvoyer
        lg = int(len(bytesaenvoyer)/2)
    #transforme texte (bytesaenvoyer) en octet (tx)
    for i in range(0, len(bytesaenvoyer)-1, 2):
        msgx.append(int("0x"+bytesaenvoyer[i:i+2], 0))
        tx2 = tx2 + "\\x" + bytesaenvoyer[i:i+2]
        tx = tx + chr(int("0x"+bytesaenvoyer[i:i+2], 0))
    if PaIp == True :
        msgx[1] = len(msgx)-2
    envoimsg(msgx)


#Ouverture de la socket d'échange et lancement du processus de thread de réception en fond de tâche
def lanceconnect():
    
    global k 
    global config
    global bulles
    global clienttcp
    ADRIP=zonedesaisieipsup.get() #Récupération de l'adresse IP 
    clienttcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #Création d'une socket d'échane 
    clienttcp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)#Fait en sorte de la socket puisse être réouverte après fermeture 
    clienttcp.connect((ADRIP, PORT)) #Connexion au client
    if k==1:           #Exécute le start du thread uniquement lors du démarrage et non à chaque appui sur le bouton 
        thread2.start()
        k=k+1
    thread2.reprise() 
    bulles=2
    tx=" Demande d'ouverture socket sur: "+ADRIP+":"+str(PORT)+" k= "+str(k)
    msgdate(tx)
    EtatSocket=Label(fenetre, text="Ouv")
    EtatSocket.grid(row=16,column=1)
    Activebulles()

        
    
#Thread qui s'execute en arrière plan et qui n'empeche pas le déroulement du programme 
#On écoute en permanance le socket afin de récuperer les données reçues      
class recepteurtcp(Thread):   

    def __init__(self, tempobulle):
        Thread.__init__(self)
        self.etat=1
    
    def run(self):
        tx="ecoute reception message tcp"
        msgdate(tx)
        clienttcp.settimeout(1)  #Permet de rendre la réception non-bloquante, si on ne reçois rien au bout de 1s on return socket.timeout
        while True :
            if self.etat==1:    #Permet de d'activer le processus d'écoute
                try:
                    datarecu=clienttcp.recv(4096) 
                    longueurliste=len(datarecu)
                    mesg=list(datarecu)
                    if longueurliste != 0:
                        trace(mesg, "Poste -> Telec. Recu : "+ str(longueurliste))
                        nature(mesg,2)
                except socket.timeout:  #Tant qu'on ne reçois rien on reboucle
                    continue
            if self.etat==2:  #Permet de stopper le processus d'écoute
                continue
                
    
    def pause(self):
        self.etat=2 
        alerte="Mise en pause du thread d'écoute : On ne reçois plus rien, tempo de 3s avant fermeture"
        msgdate(alerte)

    def reprise(self):
        self.etat=1

    

#pour envoyer un message cg "1301"
def envoiecg():
    msgseul(" demande de CG")
    TrameB32txt("1301")    
def envoyerbulle():
    #pour envoyer un message cg "1304"
    global PaIp
    global Bour
    msgseul("Message de bulle "+zonedesaisieBulles.get()+"s")
    if PaIp==True :
        messagebulle=MsgBour(Bour, 0)+MsgBour(Bour, 1)
        PaIp=False
        TrameB32txt(messagebulle)
        PaIp=True
    else:
        messagebulle="1304"
        TrameB32txt(messagebulle)

#Permet de faire un maintien de com avec le serveur avec l'envoi de bulles tout les n secondes
#Cette tâche est exécuter en arrière plan afin de ne pas bloquer le programme 
class generateurdebulle(Thread):                 

    def __init__(self, tempobulle):

        Thread.__init__(self)
        self.tempobulle = tempobulle
        global bulles
        self.exit_flag=True
        self.bulle=False

    def run(self):

        global InitBul
        global SecBul
        SecBul=0

        while True:
            if self.exit_flag:
                if bulles==1: 
                    if SecBul>0 :
                        SecBul=SecBul-1
                        time.sleep(1)
                    else:
                        if InitBul==False :
                            Init_SecBul()
                        envoyerbulle()

                elif bulles==2:
                    SecBul=0
                elif bulles==3:
                    tx="Arret de l'Application Testeur TCP"
                    msgdate(tx)
                    break                   
                else:
                    continue

    def pause(self):        #Permet l'arret du thread
        self.exit_flag=False 

    def reprise(self):     #Permet la reprise du thread
        self.exit_flag=True


def Init_SecBul():
    global SecBul
    SecBul=int(zonedesaisieBulles.get())

def clicboutonexit():
    global bulles
    bulles=3
    global config
    config['IP']['adresseip']=zonedesaisieipsup.get()
    config['IP']['tsel']=zonedesaisieTselPa.get()
    config['Messages']['msgb32']=zonedesaisieb32.get()
    config['Messages']['adtrtc']=zonedesaisieadtrtc.get()
    config['GestionIP']['bourrage']=zonedesaisieBourage.get()
    SaveIni()
    # thread2.pause()
    fenetre.destroy()
    exit()

#Permet la fermeture de la socket de communication 
def stopconnect():
    global clienttcp
    global bulles
    # thread1.pause() #Met en pause l'envoi de message bulles 
    thread2.pause() #Met en pause le thread d'écoute 
    bulles=2
    time.sleep(3)   
    clienttcp.shutdown(socket.SHUT_RDWR) 
    #clienttcp.close() #Fermeture et destruction de la socket afin de la réouvrir
    tx=" Fermeture Socket : plus d'échange possible, attente de réouverture "
    msgdate(tx)
    EtatSocket=Label(fenetre, text="Fer")
    EtatSocket.grid(row=16,column=1)
    
def Activebulles():
    global bulles
    bulles=1
    thread1.reprise()  #Lance ou relance le thread de maintien de com
    tx="Activation des messages Bulles"
    msgdate(tx)
    EtatBulles=Label(fenetre, text="Actifs")
    EtatBulles.grid(row=20,column=1)

def Desactivebulles():
    global bulles
    bulles=10
    thread1.pause() #Stop le Thread de maintien de com
    tx="desactivation des messages Bulles"
    msgdate(tx)
    EtatBulles=Label(fenetre, text="Stop")
    EtatBulles.grid(row=20,column=1)

#Commande d'enclenchement
def envoietcE():
    adtr=str(zonedesaisieadtrtc.get())
    msgseul(" TC de fermeture d'adresse trans 0x"+adtr)
    TC(adtr, "Fermeture")

#Envoyer une TC de déclenchement    
def envoietcD():
    adtr=str(zonedesaisieadtrtc.get())
    msgseul(" TC d'ouverture d'adresse trans 0x"+adtr)
    TC(adtr, "Ouverture")

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
def Miseenliaisonx25():
#exemple a envoyer pour le tsel 80 : on doit envoyer 040200000004010000000450000000 car 50=>80
    global PaIp
    global Bour
    if PaIp==False :
        bytesaenvoyer=MsgBour(Bour, 1)+MsgBour(Bour, 1)
        msgseul(" Aquit de liaison TCP par la passerelle")
        TrameB32txt(bytesaenvoyer)
        bytesaenvoyer=MsgBour(Bour, 2)+MsgBour(Bour, 1)+MsgBour(Bour, int(zonedesaisieTselPa.get()))
        msgseul(" Demande mise en liaison X25 : Tsel "+zonedesaisieTselPa.get())
        TrameB32txt(bytesaenvoyer)
        PaIp = True
        EtatX25=Label(fenetre, text="D.X25")
        EtatX25.grid(row=18,column=1)

def Misehorsliaisonx25():
    global PaIp
    if PaIp==True :
        bytesaenvoyer=MsgBour(Bour, 4)+MsgBour(Bour, 1)+MsgBour(Bour, int(zonedesaisieTselPa.get()))
        PaIp = False
        msgseul(" Demande rupture liaison X25")
        TrameB32txt(bytesaenvoyer)
        Desactivebulles()
        EtatX25=Label(fenetre, text="Arret")
        EtatX25.grid(row=18,column=1)

def MsgBour(Lng, N):
    if N>65535 :
        Lmin=3
    elif N>255 :
        Lmin=2
    else:
        Lmin=1
    if Lng<Lmin :
        Lng=Lmin
    if Lng<5 :
        Cx="0"+str(Lng)
        Lng=Lng*-2
        Tx=("00000000"+hex(N)[2:])[Lng:]
        for i in range(len(Tx)-2, -2, -2):
            Cx=Cx+Tx[i:i+2]
        return Cx

##:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#Converti l'adresse trans de la TC en message B32
# adtr : variable texte contenant l'adresse trans en hexa
# ord : Fermeture/Ouverture
def TC(adtr, ord):
    tx="19"
    
    adm=32*int("0x" + adtr, 0)+8
    if ord == "Ouverture" :
        adm = adm + 8
    mesg="19"+ ("000" + hex(adm)[2:])[-4:]
    TrameB32txt(mesg)

#Converti la liste des octets (mesg) en texte (tx)
def trace(mesg, sens):
    #mesg : Liste des octects au format "Liste integer"
    tx=""
    cx=""
    for i in range(0, len(mesg)):
        tx="0"+hex(mesg[i])[2:]
        if cx=="":
            cx=tx[len(tx)-2:]
        else:
            cx=cx+" "+tx[len(tx)-2:]
    tx="De "+sens+":\n\t" + cx[6:] + "\n"
    msgdate(tx)

def envoimsg(mesg):
    global InitBul
    global clienttcp
    if InitBul==True :
        Init_SecBul()
    trace(mesg, "Teleconduite -> Poste")
#/////// Ligne dessous à mettre en commentaire pour débuguer sans connection
    clienttcp.send(bytes(mesg))
#///////

#========================= décodage de message recu ============================================
def nature(mesg, idex):
    global PaIp
    lgm=len(mesg)
    i=idex
    if PaIp==True :
        if mesg[i+1]==8 :
            i=i+mesg[i]+1
            i=i+mesg[i]+1
            i=i+mesg[i]+1
            i=i+mesg[i]+1
        else :
            i=i+lgm

    while (i<lgm) :
        CodeNat=mesg[i]
        lng=0
        #print("code nature : "+str(CodeNat))
        if (CodeNat==10) : #code nature 0x0A
            lng=6
            adres=ADTR(mesg, i+1)
            tx=msgTx(mesg, i, lng)
            tx=tx+"   TS d'adresse "+hex(adres)+" ("+str(adres)+")"
            tx=tx+" VE="+BIT(mesg, i+2, 4, 2)
            tx=tx+" HCS="+BIT(mesg, i+2, 6, 3)
            tx=tx+" Modulo_10mn+"+str((mesg[i+3]*256+mesg[i+4])/100)+"s"
            tx=tx+" cause "+str(mesg[i+5])
            msgseul(tx)
            i=i+lng
        elif (CodeNat==98) : #code nature 0x62
            lng=7
            adres=ADTR(mesg, i+1)
            tx=msgTx(mesg, i, lng)
            tx=tx+"   TS d'adresse "+hex(adres)+" ("+str(adres)+")"
            tx=tx+" VE="+BIT(mesg, i+2, 4, 2)
            tx=tx+" HCS="+BIT(mesg, i+2, 6, 3)
            sec=60000*mesg[i+3]+(mesg[i+4]*256+mesg[i+5])
            tx=tx+" Modulo10mn="+str(mesg[i+3])+" CodeH="+str((mesg[i+3]*256+mesg[i+4])/100)+"s"
            tx=tx+"  heure="+datetime.fromtimestamp(sec/100).strftime('%H:%M:%S.%f')[:-4]
            tx=tx+" cause "+str(mesg[i+6])
            msgseul(tx)
            i=i+lng
        elif (CodeNat==22) : #code nature 0x
            lng=6
            tx=msgTx(mesg, i, lng)
            tx=tx+"  CG adresse "+hex(int(mesg[i+1]/2))+"n "
            tx=tx+" 0:VE="+BIT(mesg, i+2, 1, 2)
            tx=tx+" 1:VE="+BIT(mesg, i+2, 3, 2)
            tx=tx+" 2:VE="+BIT(mesg, i+2, 5, 2)
            tx=tx+" 3:VE="+BIT(mesg, i+2, 7, 2)
            tx=tx+" 4:VE="+BIT(mesg, i+3, 1, 2)
            tx=tx+" 5:VE="+BIT(mesg, i+3, 3, 2)
            tx=tx+" 6:VE="+BIT(mesg, i+3, 5, 2)
            tx=tx+" 7:VE="+BIT(mesg, i+3, 7, 2)
            tx=tx+" 8:VE="+BIT(mesg, i+4, 1, 2)
            tx=tx+" 9:VE="+BIT(mesg, i+4, 3, 2)
            tx=tx+" A:VE="+BIT(mesg, i+4, 5, 2)
            tx=tx+" B:VE="+BIT(mesg, i+4, 7, 2)
            tx=tx+" C:VE="+BIT(mesg, i+5, 1, 2)
            tx=tx+" D:VE="+BIT(mesg, i+5, 3, 2)
            tx=tx+" E:VE="+BIT(mesg, i+5, 5, 2)
            tx=tx+" F:VE="+BIT(mesg, i+5, 7, 2)
            msgseul(tx)
            i=i+lng
        elif (CodeNat==2) : #code nature 0x
            lng=6
            msgseul(msgTx(mesg, i, lng)+"    Télémesures cycliques par 4")
            i=i+lng
        elif (CodeNat==1) : #code nature 0x
            lng=5
            if (i+5<=lgm) : #sécurité si le message est trop petit
                msgseul(msgTx(mesg, i, lng)+"    Télémesures prioritaires")
            i=i+lng
        elif (CodeNat==3) : #code nature 0x
            lng=6
            msgseul(msgTx(mesg, i, lng)+"    Télémesures sur variation par 2")
            i=i+lng
        elif (CodeNat==26) : #code nature 0x
            lng=3+mesg[i+1]
            msgseul(msgTx(mesg, i, lng)+"    Paquet de "+str(mesg[i+1])+" Télémesures cycliques")
            i=i+lng
        elif (CodeNat==27) : #code nature 0x
            lng=2+2*+mesg[i+1]
            msgseul(msgTx(mesg, i, lng)+"    Paquet de "+str(mesg[i+1])+" Télémesures sur variation")
            i=i+lng
        elif (CodeNat==24) : #code nature 0x18
            lng=2
            msgseul(msgTx(mesg, i, lng)+"    Messages particuliers")
            i=i+lng
        elif (CodeNat==4) : #code nature 0x04
            lng=3
            msgseul(msgTx(mesg, i, lng)+"    Message de restitution TMI et TMU")
            i=i+lng
        elif (CodeNat==29) : #code nature 0x1D
            lng=5
            msgseul(msgTx(mesg, i, lng)+"    Demande de synchronisation")
            i=i+lng
        elif (CodeNat==19) : #code nature 0x13
            lng=2
            msgseul(msgTx(mesg, i, lng)+"    message de routine")
            i=i+lng
        elif (CodeNat==15) : #code nature 0x0F
            lng=2
            msgseul(msgTx(mesg, i, lng)+"    Modulo 10 minutes")
            i=i+lng
        elif (CodeNat==60) : #code nature 0x3C
            lng=5
            msgseul(msgTx(mesg, i, lng)+"    Acquit de remise à jour")
            i=i+lng
        elif (CodeNat==61) : #code nature 0x3D
            lng=5
            msgseul(msgTx(mesg, i, lng)+"    Acquit de remise à l'heure")
            i=i+lng
        elif (CodeNat==36) : #code nature 0x24
            lng=3
            msgseul(msgTx(mesg, i, lng)+"    Acquit modification TMI et TMU")
            i=i+lng
        elif (CodeNat==33) : #code nature 0x21
            lng=5
            msgseul(msgTx(mesg, i, lng)+"    Acquit d'affectation des TM prioritaires")
            i=i+lng
        elif (CodeNat==62) : #code nature 0x3E
            lng=3
            msgseul(msgTx(mesg, i, lng)+"    Acquit d'effacement d'un état anormal")
            i=i+lng
        elif (CodeNat==55) : #code nature 0x37
            lng=4
            msgseul(msgTx(mesg, i, lng)+"    Acquit Inhib ou Affect délestage")
            i=i+lng
        elif (CodeNat==9) : #code nature 0x09
            lng=3
            msgseul(msgTx(mesg, i, lng)+"    Acquit TC")
            i=i+lng
        elif (CodeNat==7) : #code nature 0x07
            lng=3+7*mesg[i+2]
            msgseul(msgTx(mesg, i, lng)+"    Paquet de "+str(mesg[i+2])+" TM 32bits")
            i=i+lng
        elif (CodeNat==0) : #code nature 0x07
            i=i+2
        else:
            if i<lgm :
                msgseul("    Erreur : Code nature inconnu")
            i=lgm
            
def msgTx(mesg, i, nb):
    tx=""
    cx=""
    for u in range(i, i+nb, 1):
        tx="0"+hex(mesg[u])[2:]
        if cx=="":
            cx=tx[len(tx)-2:]
        else:
            cx=cx+" "+tx[len(tx)-2:]
    return cx

def ADTR(mesg, i):
    #position de l'ADMOT
    #sous la forme de ADMOT+RGS
    adtr=int((256*mesg[i]+mesg[i+1])/32)
    return adtr

def BIT(mesg, i, bt, lg):
    return (bin(mesg[i])[2:].zfill(8))[bt-1:bt+lg-1]

#======================================================================================
#========================== Début du Main =============================================
#======================================================================================

# ///////////////////////// Déclaration des variables de notre projet /////////////////
FichIni="Testeur_TCP.ini"
#Lecture de la config initial
config= configparser.ConfigParser()
config.read(FichIni)
hostname = socket.gethostname()
ip = socket.gethostbyname(hostname)
adip = "Votre adresse IP est : " + str(ip)
date=datetime.now()
dx=str(date.year)+"-"+("0"+str(date.month))[len(str(date.month))-1:]+"-"+("0"+str(date.day))[len(str(date.day))-1:]
hx="_"+("0"+str(date.hour))[len(str(date.hour))-1:]+"h"+("0"+str(date.minute))[len(str(date.minute))-1:]+"min"+("0"+str(date.second))[len(str(date.second))-1:]+"s"
PaIp=False #True si c'est une messagerie passerelle
InitBul=bool(int(config['GestionIP']['Relance']))
SecBul=0
nomdufichiertrace="TRACES TESTEUR TCP "+dx+hx+".txt"
PORT = int(config['IP']['portip'])
bulles=2 #variable qui initialise l'envoi de bulles 
k=1 #Permet le start du thread2=recepteurtcp une seul fois 
#========================================================================================
#Initialisation des lancements des threads
thread1=generateurdebulle(30)
thread2=recepteurtcp(30) #tempo entre 2 messages bulles emis
thread1.start()
#///////////////////// Création et paramétrage de la fenètre d'affichage  /////////////////////////
# On crée une variable fenêtre, racine de notre interface
fenetre = Tk()
#Nom de la fenetre affiché dans windows
fenetre.title("Testeur Connexion TCP    ( version "+Version+" )")
#definition de la taille de la fenetre 
fenetre.geometry("500x430")
#/////////////// definition des variables /////////////////////////////////////////////
portsup=StringVar()
portsup.set(PORT)
ipsup=StringVar()
ipsup.set(config['IP']['adresseip'])
tselpa=StringVar()
tselpa.set(config['IP']['tsel'])
tmpBulles=StringVar()
tmpBulles.set(config['GestionIP']['tpmsgbul'])
adtc=StringVar()
adtc.set(config['Messages']['adtrtc'])
messageb32=StringVar()
messageb32.set(config['Messages']['msgb32'])
SBourage=StringVar()
SBourage.set(config['GestionIP']['bourrage'])
Bour=int(config['GestionIP']['bourrage'])
tx=" Lancement de l'application Testeur TCP Passerelle avec l'IP : "+ipsup.get()
msgdate(tx)
LigVide="======================"

                                   
#====================================Partie Graphique==============================================


#/////////////////////Définition des textes descirptifs //////////////////////////////////////////
TesteurconnexionTCP=Label(fenetre,bg='LightYellow', text="TESTEUR CONNEXION TCP")
votreip = Label(fenetre,bg='LightGreen',text=adip)
champ1=Label(fenetre,bg='LightYellow', text="Configuration de la connexion")
champ2=Label(fenetre,bg='LightYellow', text="Gestion connexion IP ")
champ3=Label(fenetre,bg='LightYellow', text="Envoie de messages B32 ")
LabTCP=Label(fenetre, text="Socket TCP ", borderwidth=1, relief="solid")
AdresseIPSUP=Label(fenetre, text="Adresse IP", borderwidth=1, relief="solid")
PortSUP=Label(fenetre, text="Port", borderwidth=1, relief="solid")
labelEnvoiemessageB32=Label(fenetre, text="Message B32", borderwidth=1, relief="solid")
labelenvoietc=Label(fenetre, text="ADTR d'une TC (hexa)", borderwidth=1, relief="solid")
lignevide=Label (fenetre, text=LigVide)
lignevide1=Label (fenetre, text=LigVide)
lignevide2=Label (fenetre, text=LigVide)
lignevide3=Label (fenetre, text=LigVide)
lignevide4=Label (fenetre, text=LigVide)
#EtatBulles=Label(fenetre, text="Arret Messages Bulles")
MsgBulles=Label(fenetre, text="Messages Bulles (second)", borderwidth=1, relief="solid")
textetselpa=Label(fenetre, text="TSEL Poste (en décimal)", borderwidth=1, relief="solid")
text2tselpa=Label(fenetre, text="En cas de Passerelle ->")
LiaisonX25=Label(fenetre, text="Mise en liaison X25 (passerelle)", borderwidth=1, relief="solid")
EtatBulles=Label(fenetre, text="Stop", borderwidth=1, relief="solid")
EtatX25=Label(fenetre, text="Arret", borderwidth=1, relief="solid")

#/////////////////////Définition des différents boutons//////////////////////////////////////////
bt_exit=Button(fenetre, text="EXIT", command=clicboutonexit)
#Bouton mise en liaison X25///////////////////////////////////////////////////////////////////
bt_MESX25=Button(fenetre, text="Demande", command=Miseenliaisonx25)
bt_MHSX25=Button(fenetre, text="Arret", command=Misehorsliaisonx25)
#boutonSET MESSAGE LIBRE
bt_semessagelibre=Button(fenetre, text="Envoyer", command=envoiemessageb32)
bt_envoietcE=Button(fenetre, text="Encl / ES", command=envoietcE)
bt_envoietcD=Button(fenetre, text="Decl / HS", command=envoietcD)
#bouton Ouverture socket
bt_ouverturesocket=Button(fenetre, text="Ouverture", command=lanceconnect)
#command=clienttcp.connect((ADRIP, PORT)))
#bouton Stop socket
bt_fermeturesocket=Button(fenetre, text="Fermeture", command=stopconnect)
#bouton Demmarage message bulles
bt_activebulles=Button(fenetre, text="Active", command=Activebulles)
#bouton STOP message bulles
bt_stopbulles=Button(fenetre, text="STOP", command=Desactivebulles)
#bouton Demande de CG
bt_CG=Button(fenetre,text="Demande de CG", command=envoiecg)


#////////////////// definition des zone de saisies /////////////////////////////////////
zonedesaisieTselPa=Entry(fenetre, textvariable=tselpa, width=4, justify="center")
zonedesaisieTselPa.bind("<Return>", execute_quand_entree_zone_TselPa)
zonedesaisieipsup=Entry(fenetre, textvariable=ipsup, width=15, justify="center")
zonedesaisieipsup.bind("<Return>", execute_quand_entree_zone_ip_sup)
zonedesaisieportsup=Entry(fenetre, textvariable=portsup, width=5, justify="center")
zonedesaisieportsup.bind("<Return>", execute_quand_entree_zone_port_sup)
zonedesaisieb32=Entry(fenetre, textvariable=messageb32, width=30)
zonedesaisieadtrtc=Entry(fenetre, textvariable=adtc, width=4, justify="center")
zonedesaisieBulles=Entry(fenetre, textvariable=tmpBulles, width=3, justify="center")
zonedesaisieBourage=Entry(fenetre, textvariable=SBourage, width=2, justify="center")
zonedesaisieBourage.bind("<Return>", entree_Bourage)


#////////////////// Placement des différents éléments dans la fenetre //////////////// 
votreip.grid(row=0,column=0)
TesteurconnexionTCP.grid(row=0,column=1)
lignevide1.grid(row=2,column=1)
champ1.grid(row=4,column=1)
AdresseIPSUP.grid(row=6,column=1,padx=5,sticky="w")
zonedesaisieipsup.grid(row=6,column=1,padx=5,sticky="e")
PortSUP.grid(row=8,column=1,padx=5,sticky="w")
zonedesaisieportsup.grid(row=8,column=1,padx=5,sticky="e")
textetselpa.grid(row=10,column=1,padx=5,sticky="w")
text2tselpa.grid(row=10,column=0,padx=10,sticky="w")
zonedesaisieTselPa.grid(row=10,column=1,padx=5,sticky="e")
lignevide2.grid(row=12,column=1)
champ2.grid(row=14,column=1)
LabTCP.grid(row=16,column=0,padx=10,sticky="w")
bt_ouverturesocket.grid(row=16,column=1,padx=10,sticky="w")
bt_fermeturesocket.grid(row=16,column=1,padx=10,sticky="e")
LiaisonX25.grid(row=18,column=0,padx=10,sticky="w")
zonedesaisieBourage.grid(row=18,column=0,padx=0,sticky="e")
bt_MESX25.grid(row=18,column=1,padx=10,sticky="w")
EtatX25.grid(row=18,column=1)
bt_MHSX25.grid(row=18,column=1,padx=10,sticky="e")
MsgBulles.grid(row=20,column=0,padx=10,sticky="w")
zonedesaisieBulles.grid(row=20,column=0,padx=0,sticky="e")
bt_stopbulles.grid(row=20,column=1,padx=10,sticky="e")
EtatBulles.grid(row=20,column=1)
bt_activebulles.grid(row=20,column=1,padx=10,sticky="w")
lignevide.grid(row=24,column=1)
champ3.grid(row=26,column=1)
bt_CG.grid(row=28,column=1)
labelEnvoiemessageB32.grid(row=30,column=0,padx=10,sticky="w")
zonedesaisieb32.grid(row=30,column=1)
bt_semessagelibre.grid(row=30,column=2,padx=10,sticky="w")
zonedesaisieadtrtc.grid(row=32,column=0,padx=10,sticky="e")
labelenvoietc.grid(row=32,column=0,padx=10,sticky="w")
bt_envoietcE.grid(row=32,column=1,padx=10,sticky="w")
bt_envoietcD.grid(row=32,column=1,padx=10,sticky="e")
lignevide4.grid(row=34,column=1)
bt_exit.grid(row=36,column=1)

# On démarre la boucle Tkinter qui s'interompt quand on ferme la fenêtre
fenetre.mainloop()
